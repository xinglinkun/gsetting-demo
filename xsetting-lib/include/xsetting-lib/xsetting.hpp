#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <gio/gio.h>

typedef GSettings XSettings;
typedef char xchar;
using namespace std;

class x_setting {
public:
    x_setting(const string schema_id);
    x_setting(const string schema_id, const string path);
    ~x_setting();

    //重载
    bool x_setting_set_value(string key, int value);

    bool x_setting_set_value(string key, unsigned int value);

    bool x_setting_set_value(string key, xchar* value);

    bool x_setting_set_value(string key, bool value);

    //获取值
    int x_setting_get_value_int(string key);

    unsigned int x_setting_get_value_uint(string key);

    string x_setting_get_value_string(string key);

    bool x_setting_get_value_bool(string key);

    vector<string> x_settings_list_keys();
    vector<string> x_settings_list_children();
    vector<string> x_settings_list_schemas_with_prefix(string prefix);
private:
    XSettings* m_setting;
};