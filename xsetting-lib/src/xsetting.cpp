#include "xsetting.hpp"

vector<string> x_setting_list_schemas()
{
    vector<string> vec;
    const xchar* const* schema = g_settings_list_schemas();
    xchar** schemas = const_cast<xchar**>(schema);
    guint schemas_size = g_strv_length(schemas);
    for (int i = 0; i < schemas_size; i++) {
        vec.push_back(schemas[i]);
    }
    return vec;
}

x_setting::x_setting(const string schema_id)
{
    m_setting = nullptr;
    const xchar* s_id = schema_id.c_str();
    m_setting = g_settings_new(s_id);
    if (m_setting == nullptr) {
        cout << "error" << endl;
    }

}

x_setting::x_setting(const string schema_id, const string path)
{
    m_setting = nullptr;
    const xchar* s_id = schema_id.c_str();
    const xchar* s_path = path.c_str();

    m_setting = g_settings_new_with_path(s_id, s_path);
    if (m_setting == nullptr) {
        cout << "error" << endl;
    }
}

x_setting::~x_setting()
{
    if (m_setting != nullptr) {
        g_object_unref(m_setting);
    }
    m_setting = nullptr;
}

//设置值
bool x_setting::x_setting_set_value(string key, int value)
{
    bool ret = g_settings_set_int(m_setting, key.c_str(), value);
    g_settings_sync();
    return ret;
}

bool x_setting::x_setting_set_value(string key, unsigned int value)
{
    bool ret = g_settings_set_uint(m_setting, key.c_str(), value);
    g_settings_sync();
    return ret;
}

bool x_setting::x_setting_set_value(string key, xchar* value)
{
    bool ret = g_settings_set_string(m_setting, key.c_str(), value);
    g_settings_sync();
    return ret;
}

bool x_setting::x_setting_set_value(string key, bool value)
{
    bool ret = g_settings_set_boolean(m_setting, key.c_str(), value);
    g_settings_sync();
    return ret;
}

//获取值
int x_setting::x_setting_get_value_int(string key)
{
    return g_settings_get_int(m_setting, key.c_str());
}

unsigned int x_setting::x_setting_get_value_uint(string key)
{
    return g_settings_get_uint(m_setting, key.c_str());
}

string x_setting::x_setting_get_value_string(string key)
{
    return g_settings_get_string(m_setting, key.c_str());
}

bool x_setting::x_setting_get_value_bool(string key)
{
    return g_settings_get_boolean(m_setting, key.c_str());
}

vector<string> x_setting::x_settings_list_keys()
{
    vector<string> vec;
    xchar** keys = g_settings_list_keys(m_setting);
    guint keys_size = g_strv_length(keys);
    for (int i = 0; i < keys_size; i++) {
        vec.push_back(keys[i]);
    }
    return vec;
}

vector<string> x_setting::x_settings_list_children()
{
    vector<string> vec;
    xchar** childrens = g_settings_list_children(m_setting);
    guint childrens_size = g_strv_length(childrens);
    for (int i = 0; i < childrens_size; i++) {
        vec.push_back(childrens[i]);
    }
    return vec;
}

vector<string> x_setting::x_settings_list_schemas_with_prefix(string prefix)
{
    vector<string> ret;
    vector<string> vec = x_setting_list_schemas();
    int size = prefix.size();
    for (const auto & str : vec) {
        if (strncmp(str.c_str(), prefix.c_str(), size) == 0) {
            ret.push_back(str);
        }
    }
    return ret;
    
}

int main(int argc, char** argv)
{
    //test
    x_setting setting("com.deepin.dde.desktop");
    for (auto &str : setting.x_settings_list_keys()) {
        cout << str << endl;
    }
    for (auto &str : setting.x_settings_list_schemas_with_prefix("com.deepin")) {
        cout << str << endl;
    }

    cout << setting.x_setting_get_value_bool("show-trash-icon") << "start" << endl;
    cout << setting.x_setting_set_value("show-trash-icon", true) << endl;
    cout << setting.x_setting_get_value_bool("show-trash-icon") << "end" << endl;
    return 0;
}